from dataclasses import dataclass, field
from datetime import datetime, time

@dataclass
class Sobrenome:
	mae: str = ""
	pai: str = ""

	def __repr__(self) -> str:
		return f"{self.mae} {self.pai}"
# class Sobrenome

@dataclass
class Nome:
	nome: str = ""
	sobrenome: Sobrenome = Sobrenome()

	def __repr__(self) -> str:
		return f"{self.nome} {self.sobrenome}"
# class Nome

@dataclass
class Cpf:
	digitos: str = ""

	def __repr__(self) -> str:
		return f"{self.digitos[0:3]}.{self.digitos[3:6]}.{self.digitos[6:9]}-{self.digitos[9:11]}"
# class Cpf

@dataclass
class Data:
	dia: int = 1
	mes: int = 1
	ano: int = 2000

	def __repr__(self) -> str:
		date = datetime(self.ano, self.mes, self.dia)
		ano  = date.strftime("%Y")
		mes  = date.strftime("%m")
		dia  = date.strftime("%d")
		return f"{dia}/{mes}/{ano}"

	def hoje(self):
		date = datetime.now()
		self.ano  = date.year
		self.mes  = date.month
		self.dia  = date.day
		return self
# class Data


@dataclass
class Naturalidade:
	estado: str = ""
	cidade: str = ""

	def __repr__(self) -> str:
		return f"{self.cidade}, {self.estado}"
# class Naturalidade


@dataclass
class Sexo:
	opcao: int = 1

	def __repr__(self) -> str:
		opcoes = {
			1: 'não informado',
			2: 'masculino',
			3: 'feminino'
		}
		return opcoes[self.opcao]
# class Sexo


@dataclass
class Pessoa:
	nome: Nome = Nome()
	cpf: Cpf = Cpf()
	nascimento: Data = Data()
	naturalidade: Naturalidade = Naturalidade()
	sexo: Sexo = Sexo()

	def __repr__(self) -> str:
		linha_1 = f"{self.nome} ({self.cpf}, de sexo {self.sexo})"
		linha_2 = f"Nascido em {self.naturalidade} - {self.nascimento}"
		return "\n".join([linha_1, linha_2])
# class Pessoa


@dataclass
class Endereco:
	rua: str = ""
	bairro: str = ""
	numero: str = ""

	def __repr__(self) -> str:
		return f"{self.rua}, número {self.numero} - bairro {self.bairro}"
# class Endereco


@dataclass
class Emails:
	emails: list[str] = field(default_factory=list)

	def __repr__(self) -> str:
		return ", ".join(self.emails)
# class Emails


@dataclass
class Cliente(Pessoa):
	endereco: Endereco = Endereco()
	emails: Emails = Emails()
	cadastro: Data = Data().hoje()

	def __repr__(self) -> str:
		linha_1 = f"Cliente: {super().__repr__()}"
		linha_2 = f"Residente em {self.endereco}"
		linha_3 = f"Teve seu registro criado em {self.cadastro}"
		linha_4 = f"E possui os seguintes emails para contato: {self.emails}"
		return "\n".join([linha_1, linha_2, linha_3, linha_4])
# class Endereco


@dataclass
class Hora:
	hora: int = 1
	minuto: int = 1

	def __repr__(self) -> str:
		tempo   = time(hour = self.hora, minute = self.minuto)
		hora    = tempo.strftime("%H")
		minuto  = tempo.strftime("%M")
		return f"{hora}:{minuto}"

	def agora(self):
		tempo = datetime.now()
		self.hora   = tempo.hour
		self.minuto = tempo.minute
		return self
# class Hora


@dataclass
class Carro:
	cor: str = ""
	modelo: str = ""
	marca: str = ""
	ano: int = 2000
	valor: int = 0

	def __repr__(self) -> str:
		return f"{self.marca} {self.modelo} {self.cor}, ano {self.ano}";
# class Carro


@dataclass
class Carros:
	carros: list[Carro] = field(default_factory=list)

	def __repr__(self) -> str:
		string  = lambda carro: carro.__repr__()
		strings = map(string, self.carros)
		return "; ".join(strings)
# class Carros


@dataclass
class Venda:
	cliente: Cliente = Cliente()
	carros: Carros = Carros()
	data: Data = Data().hoje()
	hora: Hora = Hora().agora()
	id: str = hash(f"{cliente.cpf}{data}{hora}")

	def __repr__(self) -> str:
		linha_1 = f"{self.cliente}"
		linha_2 = f"Comprou os seguintes carros: {self.carros}"
		linha_3 = f"No dia {self.data}, às {self.hora}"
		return "\n".join([linha_1, linha_2, linha_3]);
# class Venda


sobrenome    = Sobrenome("Dourado", "de Oliveira")
nome         = Nome("Ciro", sobrenome)
cpf          = Cpf("614XXXXXXXX")
nascimento   = Data(27, 8, 1996)
naturalidade = Naturalidade("Maranhão", "São Luís")
sexo         = Sexo(2)
ciro = Pessoa(nome, cpf, nascimento, naturalidade, sexo)
# print(ciro)
endereco     = Endereco("Rua XXXXXXXXXXXX", "São Francisco", "XXX")
emails       = Emails(["ciroXXXXXXX@gmail.com", "ciroXXXXXXX@hotmail.com"])
cliente      = Cliente(ciro.nome, ciro.cpf, ciro.nascimento, ciro.naturalidade, ciro.sexo, endereco, emails)
# print(cliente)
celta        = Carro("preto", "Celta", "Chevrolet", 2003, 12000)
ka           = Carro("prata", "Ka", "Ford", 2020, 35000)
carros       = Carros([ka, celta])
# print(carros)
venda        = Venda(cliente = cliente, carros = carros)
print(venda)
