<?php

class Pessoa {
  private $nome;
  private $cpf;
  private $nascimento;
  private $naturalidade;
	private $sexo;


  public function __construct()
  {
    $this->nome         = ['nome' => '', 'sobrenome' => ['mae' => '', 'pai' => '']];
    $this->cpf          = '';
    $this->nascimento   = new DateTime();
    $this->naturalidade = ['estado' => '', 'cidade' => ''];
    $this->sexo         = 1;
  }
	public static function fromArray($form)
	{
		$pessoa = new Pessoa();
    if (key_exists('nome'        , $form)) $pessoa->setNome($form['nome']);
    if (key_exists('cpf'         , $form)) $pessoa->setCpf($form['cpf']);
    if (key_exists('nascimento'  , $form)) $pessoa->setNascimento($form['nascimento']);
    if (key_exists('naturalidade', $form)) $pessoa->setNaturalidade($form['naturalidade']);
    if (key_exists('sexo'        , $form)) $pessoa->setSexo($form['sexo']);
		return $pessoa;
	}
  public function __toString()
  {
    $nome         = $this->formatNome();
    $cpf          = $this->formatCpf();
    $naturalidade = $this->formatNaturalidade();
    $nascimento   = $this->formatNascimento();
		$sexo         = $this->formatSexo();
    return join(PHP_EOL, [
			"$nome ($cpf, de sexo $sexo)",
			"Nascido em $naturalidade - $nascimento"
		]);
  }


  public function getNome()      { return $this->nome; }
  public function setNome($nome) {
    if (key_exists('nome'     , $nome)) $this->nome['nome'] = $nome['nome'];
    if (key_exists('sobrenome', $nome)) {
			if (key_exists('mae', $nome['sobrenome']))
				$this->nome['sobrenome']['mae'] = $nome['sobrenome']['mae'];
			if (key_exists('pai', $nome['sobrenome']))
				$this->nome['sobrenome']['pai'] = $nome['sobrenome']['pai'];
		}
	}
  public function formatNome() {
    $nome         = $this->nome['nome'];
    $sobrenomeMae = $this->nome['sobrenome']['mae'];
    $sobrenomePai = $this->nome['sobrenome']['pai'];
    return join(' ', [$nome, $sobrenomeMae, $sobrenomePai]);
  }


  public function getCpf()     { return $this->cpf; }
  public function setCpf($cpf) { $this->cpf = $cpf; }
  public function formatCpf() {
    return
      substr($this->cpf, 0, 3) . "." .
      substr($this->cpf, 3, 3) . "." .
      substr($this->cpf, 6, 3) . "-" .
      substr($this->cpf, 9, 2);
  }


  public function getNascimento()      { return $this->cpf; }
  public function setNascimento($data) {
    $this->nascimento = DateTime::createFromFormat('d/m/Y', $data);
  }
  public function formatNascimento() {
    return $this->nascimento->format('d/m/Y');
  }


  public function getNaturalidade()              { return $this->naturalidade; }
  public function setNaturalidade($naturalidade) {
    if (key_exists('estado', $naturalidade)) $this->naturalidade['estado'] = $naturalidade['estado'];
    if (key_exists('cidade', $naturalidade)) $this->naturalidade['cidade'] = $naturalidade['cidade'];
  }
  public function formatNaturalidade() {
    $estado = ucfirst($this->naturalidade['estado']);
    $cidade = ucfirst($this->naturalidade['cidade']);
    return "$cidade, $estado";
  }


  public function getSexo()      { return $this->sexo; }
  public function setSexo($sexo) { $this->sexo = $sexo; }
  public function formatSexo() {
		$opcoes = [
			1 => 'não informado',
			2 => 'masculino',
			3 => 'feminino'
		];
    return $opcoes[$this->sexo];
  }


	public function andar()  { echo 'Andando até minha casa'; }
	public function falar()  { echo 'Olá!'; }
	public function comer()  { echo 'Gnocchi, por favor'; }
	public function dormir() { echo 'Zzz'; }
}


// $form = [
// 	'nome'         => ['nome' => 'Ciro', 'sobrenome' => ['mae' => 'Dourado', 'pai' => 'de Oliveira']],
// 	'cpf'          => '614XXXXXXXX',
// 	'nascimento'   => '27/08/1996',
// 	'naturalidade' => ['estado' => 'Maranhão', 'cidade' => 'São Luís'],
// 	'sexo'         => 2,
// ];
// $ciro = Pessoa::fromArray($form);
// echo $ciro;


class Cliente extends Pessoa {
  private $endereco;
  private $emails;
	private $cadastro;


  public function __construct()
  {
    parent::__construct();
		$this->endereco = ['rua' => '', 'bairro' => '', 'numero' => ''];
		$this->emails   = [];
		$this->cadastro = new DateTime('now', new DateTimeZone('America/Fortaleza'));
  }
	public static function fromArray($form)
	{
		$cliente = new Cliente();
    if (key_exists('nome'        , $form)) $cliente->setNome($form['nome']);
    if (key_exists('cpf'         , $form)) $cliente->setCpf($form['cpf']);
    if (key_exists('nascimento'  , $form)) $cliente->setNascimento($form['nascimento']);
    if (key_exists('naturalidade', $form)) $cliente->setNaturalidade($form['naturalidade']);
    if (key_exists('sexo'        , $form)) $cliente->setSexo($form['sexo']);
    if (key_exists('endereco'    , $form)) $cliente->setEndereco($form['endereco']);
    if (key_exists('emails'      , $form)) $cliente->setEmails($form['emails']);
		return $cliente;
	}
  public function __toString()
  {
		$pessoais = parent::__toString();
    $endereco = $this->formatEndereco();
    $cadastro = $this->formatCadastro();
    $emails   = $this->formatEmails();

    return join(PHP_EOL, [
			"Cliente: $pessoais",
			"Residente em $endereco",
			"Teve seu registro criado em $cadastro",
			"E possui os seguintes emails para contato: $emails"
		]);
  }


  public function getEndereco()          { return $this->endereco; }
  public function setEndereco($endereco) {
    if (key_exists('rua'   , $endereco)) $this->endereco['rua']    = $endereco['rua'];
    if (key_exists('bairro', $endereco)) $this->endereco['bairro'] = $endereco['bairro'];
    if (key_exists('numero', $endereco)) $this->endereco['numero'] = $endereco['numero'];
  }
  public function formatEndereco() {
		$rua    = $this->endereco['rua'];
		$bairro = $this->endereco['bairro'];
		$numero = $this->endereco['numero'];
    return "$rua, número $numero - bairro $bairro";
  }


  public function formatCadastro() {
    return $this->cadastro->format('d/m/Y');
  }


  public function getEmails()        { return $this->emails; }
  public function setEmails($emails) {
		foreach ($emails as $email)
			array_push($this->emails, $email);
  }
  public function formatEmails() {
		return join(', ', $this->emails);
  }


	public function andar() { echo 'Andando até a concessionária'; }
	public function falar() { echo 'Uma boa tarde.'; }
	public function comprar($carros) {
		$dados = [
			'cliente' => $this,
			'carros'  => $carros
		];
		return Venda::fromArray($dados);
	}
}


// $form = [
// 	'nome'         => ['nome' => 'Ciro', 'sobrenome' => ['mae' => 'Dourado', 'pai' => 'de Oliveira']],
// 	'cpf'          => '614XXXXXXXX',
// 	'nascimento'   => '27/08/1996',
// 	'naturalidade' => ['estado' => 'Maranhão', 'cidade' => 'São Luís'],
// 	'sexo'         => 2,
// 	'endereco'     => ['rua' => 'Rua XXXXXXXXXXXX', 'bairro' => 'São Francisco', 'numero' => 'XXX'],
// 	'emails'       => ['ciroXXXXXXX@gmail.com']
// ];
// $ciro = Cliente::fromArray($form);
// echo $ciro;


class Venda {
  private $id;
  private $cliente;
	private $carros;
	private $data;
	private $hora;


  public function __construct()
  {
		$agora    = new DateTime('now', new DateTimeZone('America/Fortaleza'));
		$cpf      = "XXXXXXXXXXX";
		$instante = $agora->getTimeStamp();

		$this->cliente = null;
		$this->carros  = [];
		$this->data    = $agora;
		$this->hora    = $agora;
		$this->id      = hash('sha256', "$cpf $instante");
  }
	public static function fromArray($form)
	{
		$venda = new Venda();
    if (key_exists('cliente', $form)) $venda->setCliente($form['cliente']);
    if (key_exists('carros' , $form)) $venda->setCarros($form['carros']);
		$venda->setId();
		return $venda;
	}
  public function __toString()
  {
    $cliente  = $this->formatCliente();
    $carros   = $this->formatCarros();
    $data     = $this->formatData();
    $hora     = $this->formatHora();

    return join(PHP_EOL, [
			$cliente,
			"Comprou os seguintes carros: $carros",
			"No dia $data, às $hora"
		]);
  }


  public function getCliente()         { return $this->cliente; }
  public function setCliente($cliente) { $this->cliente = $cliente; }
  public function formatCliente()      { return $this->cliente->__toString(); }


  public function getCarros()        { return $this->carros; }
  public function setCarros($carros) {
		foreach ($carros as $carro)
			array_push($this->carros, $carro);
	}
  public function formatCarros() {
		$string  = fn($carro) => $carro->__toString();
		$strings = array_map($string, $this->carros);
		return join('; ', $strings);
	}


  public function formatData() {
    return $this->data->format('d/m/Y');
  }
  public function formatHora() {
    return $this->hora->format('H:i');
  }
  public function setId() {
		$cpf      = $this->cliente->getCpf();
		$instante = $this->data->getTimeStamp();
		$this->id = hash('sha256', "$cpf $instante");
  }


	public function cliente()      { return $this->getCliente(); }
	public function carro($indice) { return $this->carros[$indice] ?? null;	}
}


// $formCliente = [
// 	'nome'         => ['nome' => 'Ciro', 'sobrenome' => ['mae' => 'Dourado', 'pai' => 'de Oliveira']],
// 	'cpf'          => '614XXXXXXXX',
// 	'nascimento'   => '27/08/1996',
// 	'naturalidade' => ['estado' => 'Maranhão', 'cidade' => 'São Luís'],
// 	'sexo'         => 2,
// 	'endereco'     => ['rua' => 'Rua XXXXXXXXXXXX', 'bairro' => 'São Francisco', 'numero' => 'XXX'],
// 	'emails'       => ['ciroXXXXXXX@gmail.com']
// ];
// $formVenda = [
// 	'cliente' => Cliente::fromArray($formCliente),
// 	'carros'  => []
// ];
// $venda = Venda::fromArray($formVenda);
// echo $venda;


class Carro {
  private $cor;
  private $modelo;
	private $marca;
	private $ano;
	private $valor;


  public function __construct()
  {
		$this->cor    = "";
		$this->modelo = "";
		$this->marca  = "";
		$this->ano    = 2000;
		$this->valor  = 0;
  }
	public static function fromArray($form)
	{
		$carro = new Carro();
    if (key_exists('cor'   , $form)) $carro->setCor($form['cor']);
    if (key_exists('modelo', $form)) $carro->setModelo($form['modelo']);
    if (key_exists('marca' , $form)) $carro->setMarca($form['marca']);
    if (key_exists('ano'   , $form)) $carro->setAno($form['ano']);
    if (key_exists('valor' , $form)) $carro->setValor($form['valor']);
		return $carro;
	}
  public function __toString()
  {
    $cor    = $this->formatCor();
    $modelo = $this->formatModelo();
    $marca  = $this->formatMarca();
    $ano    = $this->formatAno();

    return "$marca $modelo $cor, ano $ano";
  }


  public function getCor()     { return $this->cor; }
  public function setCor($cor) { $this->cor = $cor; }
  public function formatCor()  { return strtolower($this->cor); }


  public function getModelo()        { return $this->modelo; }
  public function setModelo($modelo) { $this->modelo = $modelo; }
  public function formatModelo()     { return ucfirst(strtolower($this->modelo)); }


  public function getMarca()       { return $this->marca; }
  public function setMarca($marca) { $this->marca = $marca; }
  public function formatMarca()    { return ucfirst(strtolower($this->marca)); }


  public function getAno()     { return $this->ano; }
  public function setAno($ano) { $this->ano = $ano; }
  public function formatAno()  { return strval($this->ano); }


  public function getValor()       { return $this->valor; }
  public function setValor($valor) { $this->valor = $valor; }
  public function formatValor()    { return "R$ " . strval($this->valor); }


	public function andar() { echo "Acelerando"; }
	public function frear() { echo "Freando"; }
}


// $form = [
// 	'cor'    => "Prata",
// 	'modelo' => 'ka',
// 	'marca'  => 'ford',
// 	'ano'    => 2020,
// 	'valor'  => 35000,
// ];
// $carro = Carro::fromArray($form);
// echo $carro;


$formCarro1 = [
	'cor'    => "Prata",
	'modelo' => 'ka',
	'marca'  => 'ford',
	'ano'    => 2020,
	'valor'  => 35000,
];
$formCarro2 = [
	'cor'    => "preto",
	'modelo' => 'Celta',
	'marca'  => 'Chevrolet',
	'ano'    => 2003,
	'valor'  => 12000,
];
$formCliente = [
	'nome'         => ['nome' => 'Ciro', 'sobrenome' => ['mae' => 'Dourado', 'pai' => 'de Oliveira']],
	'cpf'          => '614XXXXXXXX',
	'nascimento'   => '27/08/1996',
	'naturalidade' => ['estado' => 'Maranhão', 'cidade' => 'São Luís'],
	'sexo'         => 2,
	'endereco'     => ['rua' => 'Rua XXXXXXXXXXXX', 'bairro' => 'São Francisco', 'numero' => 'XXX'],
	'emails'       => ['ciroXXXXXXX@gmail.com', 'ciroXXXXXXX@hotmail.com']
];
$formVenda = [
	'cliente' => Cliente::fromArray($formCliente),
	'carros'  => [Carro::fromArray($formCarro1), Carro::fromArray($formCarro2)]
];
$venda = Venda::fromArray($formVenda);
echo $venda . PHP_EOL;
