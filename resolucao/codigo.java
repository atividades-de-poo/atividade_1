import java.util.HashMap;
import java.util.ArrayList;
import java.time.LocalDate;
import java.time.LocalTime;
import java.math.BigDecimal;


public class Pessoa {
	// atributos
	private HashMap<String, String> nome;
	private LocalDate               nascimento;
	private String                  cpf;
	private HashMap<String, String> naturalidade;
	private int                     sexo;

	// construtor default
	public Pessoa() {}

	// métodos
	public void falar() {}
	public void andar() {}
	public void comer() {}
	public void dormir() {}
}


public class Cliente extends Pessoa
	// isso significa que a classe cliente herda os mesmos atributos de Pessoa, ou
	// seja, um cliente também vai possuir nome, cpf, data de nascimento, dentre
	// outras informações, sem que tenhamos de redeclarar todas elas
{
	// atributos
	private HashMap<String, String> endereco;
	private ArrayList<String>       emails;
	private LocalDate               cadastro;

	// construtor default
	public Cliente() {
		// aqui chamamos explicitamente o construtor da classe-pai. Geralmente isso
		// não é necessário, salvo quando temos variantes do construtor, que aceitam
		// diversos tipos de argumentos. Isso inicializará a classe pai com condições
		// iniciais bem definidas
		super();
	}

	// métodos
	// aqui temos algo diferente também. Override é uma anotação especial que
	// indica que este método está sendo herdado de alguma classe-pai, e que
	// está sendo reescrito. Além de herança, temos aqui um exemplo de
	// polimorfismo. No geral overrides funcionam sem esta anotação.
	@Override public void falar() {}
	@Override public void andar() {}
	public Venda comprar(ArrayList<Carro> carros) { return null; }

	// vale lembrar que além de herdar os atributos, e poder reescrever a lógica
	// de métodos herdados, a classe filha tem total liberdade para chamar os
	// métodos da classe pai
}


public class Venda {
	// atributos
	private String           id;
	private Cliente          cliente;
	private ArrayList<Carro> carros;
	private LocalDate        data;
	private LocalTime        hora;

	public Venda() {
		// construtor padrão
	}
	public Venda(Cliente cliente, ArrayList<Carro> carros) {
		// construtor parametrizado
		this.cliente = cliente;
		this.carros  = carros;
		// ...
	}
	// acima temos novamente o conceito de polimorfismo, mas desta vez se trata de
	// métodos que possuem o mesmo nome, no mesmo escopo de classe (e não de classes
	// diferentes, como na herança). Aqui, temos duas funções que realizam a mesma tarefa
	// mas que o fazem em procedimentos diferentes

	// além disso, temos o que se conhece por dependência: a classe venda é responsável
	// apenas por relacionar um objeto cliente com um ou mais objetos carro, representando
	// justamente uma venda, onde os carros e o cliente em questão fazem parte do mesmo escopo

	// aqui ocorre apenas uma junção de objetos já criados
	// nada de novo é criado aqui dentro, em relação a carros ou clientes

	// métodos
	public Cliente cliente()         { return $this->cliente; }
	public Carro   carro(int indice) { return null; }
}


public class Carro {
	// atributos
	private String     cor;
	private String     modelo;
	private String     marca;
	private int        ano;
	private BigDecimal valor;

	public Carro() {
		// construtor padrão
	}

	// métodos
	public void andar() {}
	public void frear() {}

	// por fim, temos um método já visto antes em outras classes chamado "andar";
	// e apesar destas coompartilharem o mesmo nome, o método andar da classe carro
	// não gera algum problema em especial, visto que ele existe apenas no contexto de
	// um carro: é tratado diferente, não é herdado, e muito menos possui alguma implementação
	// de alguma superclasse
}
